#!/usr/bin/python
#coding:utf-8

import requests
from bs4 import BeautifulSoup
import bs4
import time
from gif_data import GifData
import video_handler as videoHandler
import os
from gif_logger import logger as gLogger
from datetime import datetime, timedelta
import json
import codecs

HOST = 'http://www.gifbin.com'
SEARCH_HOST = '%s/search' % HOST
MAX_PAGE = None
DOWNLOAD_DIR = './source/mp4'


def search_gif_with_tag(duration=100, tag='funny'):
    global MAX_PAGE
    if not MAX_PAGE:
        url = '%s/%s/%s' % (SEARCH_HOST, tag, '1')
        r = requests.get(url)
        cur_page = 1
        if r.status_code == 200:
            soup = BeautifulSoup(r.content)
            pageination = soup.select('div.pagination')
            last_pageination = pageination[0].contents[-1]
            MAX_PAGE = int(last_pageination.text) or 156
            # 获取检查点最后一页
            last_check_page_js = {}
            with open('./gif_bin_collector_page.json') as f:
                last_check_page_js = json.load(f)
            last_page = last_check_page_js['last_check_page']
            cur_page = max(last_page, cur_page)

            t1 = datetime.now()
            is_continue = True

            if cur_page > MAX_PAGE:
                cur_page = 1
            while cur_page < MAX_PAGE and is_continue:
                try:
                    _search_gif_with_tag(tag, cur_page)
                    # 当前页+1 并且保存到检查点里面
                    cur_page += 1
                    last_check_page_js['last_check_page'] = cur_page
                    fp = codecs.open('./gif_bin_collector_page.json', 'w+', 'utf-8')
                    fp.write(json.dumps(last_check_page_js, ensure_ascii=False, indent=4))
                except Exception as e:
                    print ('获取当前页出错')
                finally:
                    t2 = datetime.now()
                    total_sec = (t2 - t1).total_seconds()
                    is_continue = total_sec < duration


def _search_gif_with_tag(tag='funny', page_num=1):
    def get_info(soup_tag):
        contents = soup_tag.contents
        id = contents[1].find_all('a')[0]['href'].split('/')[1]
        img_tag = contents[1].find_all('img')[0]
        gif_relateiv_url = img_tag['src']
        gif_url = '%s%s' % (HOST, gif_relateiv_url)
        title = img_tag['alt']
        return id, gif_url, title

    def download(url, savepath):
        try:
            gif_path = os.path.dirname(url)
            gif_name = os.path.basename(url)
            if gif_name.startswith('tn_'):
                gif_name = gif_name.replace('tn_', '')
            url = os.path.join(gif_path, gif_name)
            response = requests.get(url)
            if response.status_code == 200:
                gLogger.info('下载 URL %s 成功' % url.encode('utf8'))
                with open(savepath, 'wb') as f:
                    f.write(response.content)
                    return True
            else:
                gLogger.error('下载 URL %s 失败' % url.encode('utf8'))
                return False
        except Exception as e:
            gLogger.error('下载GIF失败!休息个5秒后继续下载!')
            time.sleep(5)
            return False

    def need_download(save_dir, local_gif_name):
        if os.path.exists(os.path.join(save_dir, local_gif_name)):
            gLogger.info('本地GIF存在，不下载')
            return False
        return True

    url = '%s/%s/%s' % (SEARCH_HOST,tag, page_num)
    r = requests.get(url)
    gLogger.info('搜索Gif数据 %s' % url)
    global DOWNLOAD_DIR
    if r.status_code == 200:
        soup = BeautifulSoup(r.content)
        thumbs = soup.select('ul.browse-thumbs.clearfix')
        gif_items = thumbs[0].contents
        for item in gif_items:
            try:
                if isinstance(item, bs4.element.Tag):
                    time.sleep(2)
                    id, gif_url, title = get_info(soup_tag=item)
                    original_gif_name = '%s.gif' % id
                    local_gif_path = '%s/%s' % (DOWNLOAD_DIR, original_gif_name)
                    local_mp4_name = '%s.mp4' % id
                    local_mp4_path = '%s/%s' % (DOWNLOAD_DIR, local_mp4_name)
                    download_success = True
                    if GifData.query_is_release(id):
                        gLogger.info('MP4已经发布，不下载')
                        continue
                    if need_download(DOWNLOAD_DIR, original_gif_name):
                        download_success = download(gif_url, local_gif_path)
                    if download_success:
                        if not os.path.exists(local_mp4_path):
                            videoHandler.gif_to_mp4(local_gif_path, local_mp4_path)
                        duration = videoHandler.mp4_info(local_mp4_path)['duration']
                        info = {'id':id, 'tag':tag, 'duration':duration, 'title':title}
                        gif_item = GifData(id=id,
                                           local_path=local_mp4_path,
                                           web_path=url,
                                           text=title,
                                           duration=duration,
                                           isrelease=False,
                                           isDownload=download_success,
                                           info= str(info)
                                           )
                        gif_item.inserOrUpdate()
            except Exception as e:
                print (e)


# _search_gif_with_tag()
search_gif_with_tag(duration=600)
