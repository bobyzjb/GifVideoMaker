#!/usr/bin/python
#coding:utf-8

import os
from mutagen.mp3 import MP3
import subprocess
import json
from PIL import Image, ImageSequence

lang_dic = {'chinese':'PingFang.ttc',
            'eng':'Sathu.ttf'}

MP3_DIR = './source/mp3/'
MP4_DIR = './source/mp4/'
TEXT_MP4_DIR = './source/text_mp4/'
SCALE_MP4_DIR = './source/scale_mp4/'
TMP_TS_MP4_DIR = './source/ts_mp4/'
CONCAT_OUTPUT_DIR = './source/concat_mp4/'
MIX_MP4_DIR = './source/mix_mp4/'

def gif_to_mp4(gif_path, mp4_path):
    cmd = "ffmpeg -y -i %s  -movflags faststart  -pix_fmt yuv420p -vf \"scale=trunc(iw/2)*2:trunc(ih/2)*2\" %s" \
          % (gif_path, mp4_path)
    print(cmd)
    os.system(cmd)

def add_text(mp4_path, text, out_put_path=None, text_lang='chinese'):
    font_path = lang_dic[text_lang]
    # x= (w - t * 50):y = (h - th) / 2
    # x = (W / 2):y = trunc(H - 20)
    # -vcodec mpeg4
    cmd = "ffmpeg -y -i %s -vf \"drawtext=fontfile=%s: text=\'%s\':x= (w - t * 50):y = (h - th - 20):fontsize=24:fontcolor=yellow:shadowy=2\"  %s" \
          % (mp4_path, font_path, text, out_put_path)
    print(cmd)
    os.system(cmd)
    pass


def concat_mp4files(input_paths, out_put_path, tmp_ts_dir=TMP_TS_MP4_DIR):
    if not isinstance(input_paths, list):
        raise Exception('Files should be list')
    concat_ts = []
    for input_path in input_paths:
        if not os.path.exists(input_path):
            print('%s not exit' % input_path)
            continue
        else:
            file_name = os.path.split(input_path)[1]
            file_simple_name = os.path.splitext(file_name)[0]
            cmd = "ffmpeg -y -i %s -c copy -bsf:v h264_mp4toannexb -f mpegts %s.ts " \
                  % (input_path, tmp_ts_dir + file_simple_name )
            concat_ts.append(tmp_ts_dir + file_simple_name+'.ts')
            os.system(cmd)
    cmd = 'ffmpeg -y -i "concat:%s" -c copy -bsf:v h264_mp4toannexb %s' \
          % ('|'.join(concat_ts), out_put_path)
    os.system(cmd)


def add_music(mp4_file_path, music_path, output_path):
    cmd = "ffmpeg -y -i '%s' -i '%s' -codec copy -shortest %s " % (mp4_file_path, music_path, output_path)
    os.system(cmd)


def mp3_info(mp3_file_path):
    if not os.path.exists(mp3_file_path):
        raise Exception('MP3 FILE NOT EXIT')
    audio = MP3(mp3_file_path)
    tags = audio.tags
    music_name = str(tags['TIT2'])
    singer = str(tags['TPE1'])
    album = str(tags['TALB'])
    length = audio.info.length
    return {'music_name':music_name, 'singer':singer, 'album':album, 'length':length}


def mp4_info(mp4_file_path):
    try:
        command = ["ffprobe",
                   "-loglevel", "quiet",
                   "-print_format", "json",
                   "-show_format",
                   "-show_streams",
                   mp4_file_path
                   ]
        result = subprocess.Popen(command,
                                  stdout=subprocess.PIPE,
                                  stderr=subprocess.STDOUT)
        out, err = result.communicate()
        # duation = float((json.loads(out))['format']['duration'])
        streams = (json.loads(out))['streams'][0]
        width = streams['width']
        height = streams['height']
        duration = float(streams['duration_ts']/10000)
        return {'duration': duration, 'width': width, 'height': height}
    except Exception as e:
        print(e)
        return 0


def gif_duration(gif_path):
    try:
        im = Image.open(gif_path)
    except IOError as err:
        print('%s: Error' % os.path.basename(gif_path))
        return
    durations = []
    for frame in ImageSequence.Iterator(im):
        try:
            durations.append(frame.info['duration'])
        except KeyError:
            # Ignore if there was no duration, we will not count that frame.
            pass

    if not durations:
        print('Not an animated GIF image')
    else:
        total_duration = sum(durations)
        # print('Total duration: %d ms (%0.2f seconds)' % (total_duration, total_duration / 1000.0))
        return total_duration


def scale_mp4(input_path, output_path, targetWH):
    if not os.path.exists(input_path):
        raise Exception('INPUT FILE NOT EXIT')
    wh = targetWH.split(':')
    if not len(wh) == 2:
        raise Exception('Illegial Params of width and height')
    target_width = int(wh[0])
    target_height = int(wh[1])
    info = mp4_info(input_path)
    width = info['width']
    height = info['height']
    # scale = '-1:%d' % target_height if min(width, height) == width else '%d:-1' % target_width
    # if min(width, height) == width else '0:(%d-ih/2)' % (target_height)
    position = '(%d-iw)/2:(%d-ih)/2' % (target_width, target_height)

    video_scale = min(target_width/width, target_height/height)
    target_width = int(video_scale * width)
    target_height = int(video_scale * height)
    scale = '%d:%d' % (target_width, target_height)
    cmd = "ffmpeg -y -i '%s'  -vf 'scale=%s,pad=%s:%s:black' '%s' " % (input_path, scale, targetWH, position, output_path)
    os.system(cmd)

# gif_to_mp4('./source/mp4/990319.gif','./source/mp4/990319.MP4')
# add_text('./source/mp4/fish-bites-man-39-s-nipple.mp4', 'di', './source/text_mp4/text_fish-bites-man-39-s-nipple.mp4')
# add_text('./source/mp4/1520860568.mp4', 'di', './source/text_mp4/text_1520860568.mp4')
# add_music('./source/text_mp4/text_1517989768.mp4', './source/mp3/zjl_xy.mp3', './source/mix_mp4/mix_1517989768.mp4')
# add_music('./source/text_mp4/text_1517989768.mp4', './source/mp3/xxy.mp3', './source/mix_mp4/mix_1517989768.mp4')
#
# concat_mp4files([TEXT_MP4_DIR+'text_1520003203.mp4',
#                  TEXT_MP4_DIR+'text_1520860568.mp4'],
#                 'output.mp4')

# mp3_info(MP3_DIR+'xxy.mp3')
# mp4_info('/Users/stev/Documents/PycharmProj/GifVideoMaker/source/mp4/fish-bites-man-39-s-nipple.mp4')
# gif_duration('/Users/stev/Documents/PycharmProj/GifVideoMaker/source/mp4/fish-bites-man-39-s-nipple.mp4')
# scale_final_mp4('./source/mix_mp4/final.mp4','./source/mix_mp4/r_final2.mp4')

# h > w
# scale_mp4('./source/text_mp4/1520003203.mp4', './source/scale_mp4/1520003203.mp4', '1280:800')
# w > h
# scale_mp4('./source/text_mp4/1518924081.mp4', './source/scale_mp4/1518924081.mp4', '1280:800')
