#!/usr/bin/python
#coding:utf-8

import json
import logging.config
import os

import sys
reload(sys)
sys.setdefaultencoding("utf-8")

def setup_logging(default_path=os.path.dirname(__file__)+'/logconfig.json', default_level=logging.INFO, env_key='log_cfg'):
    path = default_path
    value = os.getenv(env_key, None)
    if value:
        path = value
    if os.path.exists(path):
        with open(path,'rt') as f:
            config = json.load(f)
        logging.config.dictConfig(config)
    else:
        logging.basicConfig(level=default_level)
    pass



# 这里控制了默认的输出级别
setup_logging(default_level=logging.DEBUG)
logger = logging.getLogger(__name__)



# logger.debug('傻逼')
# logger.info('傻逼1')
# logger.error('傻逼2')

