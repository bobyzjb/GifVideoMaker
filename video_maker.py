#!/usr/bin/python
# coding:utf-8

import video_handler as vh
import gifsource_collector as gif_clter
import os
import json
from random import choice
from lang import translate
import codecs
from gif_logger import logger as gLogger
from gif_data import GifData
MUSIC_COUNT = 1
MP3_DIR = vh.MP3_DIR
MP4_DIR = vh.MP4_DIR
TEXT_MP4_DIR = vh.TEXT_MP4_DIR
SCALE_MP4_DIR = vh.SCALE_MP4_DIR
TMP_TS_MP4_DIR = vh.TMP_TS_MP4_DIR
CONCAT_OUTPUT_DIR = vh.CONCAT_OUTPUT_DIR
MIX_MP4_DIR = vh.MIX_MP4_DIR

CONFIG_PATH = './config.json'

# 预处理
# 删除JSON 文件
# 删除 MP4 文件


def delete_files_in_dir(dir):
    for mp4_item in os.listdir(dir):
        mp4_file_path = os.path.join(dir, mp4_item)
        os.remove(mp4_file_path)


if __name__ == '__main__':
    config = {}
    with open(CONFIG_PATH) as f:
        config = json.load(f)
    lang = config['lang']

    # 暂时不 delete MP4 FILE，这样就可以不停继续下载而不重复
    # delete_files_in_dir(MP4_DIR)
    delete_files_in_dir(TEXT_MP4_DIR)
    delete_files_in_dir(TMP_TS_MP4_DIR)
    delete_files_in_dir(CONCAT_OUTPUT_DIR)
    delete_files_in_dir(MIX_MP4_DIR)
    # 删除已经发布的Gif数据
    GifData.remove_files_release()

    # 选取音乐
    musics_files = [f for f in os.listdir(MP3_DIR) if os.path.isfile(os.path.join(MP3_DIR, f))]
    pend_music_files = musics_files[0: MUSIC_COUNT]

    # 获取音乐长度 ( 7分钟 到 11分钟 )
    total_music_length = 0
    pend_music_info_dic = {}
    for music_file in pend_music_files:
        music_file_path = os.path.join(MP3_DIR, music_file)
        music_info = vh.mp3_info(music_file_path)
        pend_music_info_dic[music_file_path] = music_info
        total_music_length += music_info['length']



    # # 删除文件夹里所有的文件
    # # 下载 gif total_music_length
    local_unrelease_total_time = GifData.query_unrelease_total_time()
    if local_unrelease_total_time < total_music_length:
        gLogger.error('未发布MP4时间小于音乐时长，无法生成视频')
        raise Exception('未发布MP4时间小于音乐时长')

    l = GifData.query_unrelease_gifs()
    if not len(l):
        gLogger.error('没有未发布的MP4数据，无法生成视频')
        raise Exception('没有未发布数据')

    curtotal_duration = 0
    pend_items = []
    for item in l:
        if os.path.exists(item['local_path']):
            curtotal_duration += item['duration']
            pend_items.append(item)
            if curtotal_duration >= total_music_length:
                break
    if curtotal_duration < total_music_length:
        gLogger.error('未发布MP4时间小于音乐时长，无法生成视频')
        raise Exception('未发布MP4时间小于音乐时长')

    concat_mp4_files = []
    for pend_item in pend_items:
        id = pend_item['id']
        local_mp4_path = pend_item['local_path']
        mp4_file_name = os.path.split(local_mp4_path)[1]
        interesting_comment = pend_item['text'] or ''
        out_text_mp4_path = os.path.join(TEXT_MP4_DIR, mp4_file_name)
        out_scale_mp4_path = os.path.join(SCALE_MP4_DIR, mp4_file_name)
        vh.add_text(local_mp4_path, interesting_comment, out_text_mp4_path)
        vh.scale_mp4(out_text_mp4_path, out_scale_mp4_path, '1280:800')
        concat_mp4_files.append(out_scale_mp4_path)

    # gif_infos = gif_clter.collect_gifs(MP4_DIR, target_duation=total_music_length)
    # concat_mp4_files = []
    # for gif_info in gif_infos:
    #     mp4_file_name = gif_info.keys()[0]
    #     gif_dict = gif_info.values()[0]
    #     interesting_comment = gif_dict['interesting_comment'].encode('utf8') or ''
    #     local_mp4_path = gif_dict['local_mp4_path']
    #     out_text_mp4_path = os.path.join(TEXT_MP4_DIR, mp4_file_name)
    #     out_scale_mp4_path = os.path.join(SCALE_MP4_DIR, mp4_file_name)
    #     # 为每一个 gif 加上文字
    #     vh.add_text(local_mp4_path, interesting_comment, out_text_mp4_path)
    #     vh.scale_mp4(out_text_mp4_path, out_scale_mp4_path, '1280:800')
    #     concat_mp4_files.append(out_scale_mp4_path)

    # 拼接所有 gif
    concat_mp4_output_path = os.path.join(CONCAT_OUTPUT_DIR, 'output.mp4')
    vh.concat_mp4files(concat_mp4_files,
                       concat_mp4_output_path,
                       TMP_TS_MP4_DIR)

    # 生成视频 暂时只拿第一首os.path.join(MP3_DIR,  pend_music_files[0])
    mp3_path = pend_music_info_dic.keys()[0]
    final_mp4_output_path = os.path.join(MIX_MP4_DIR, 'final.mp4')
    vh.add_music(concat_mp4_output_path, mp3_path, final_mp4_output_path)
    os.remove(mp3_path)


    upload_music_info = pend_music_info_dic.values()[0]
    # {'music_name': music_name, 'singer': singer, 'album': album, 'length': length}

    count = config['count']
    upload_set = config[lang]
    # 获取随机标题
    title_prefix = choice(upload_set['title_prefix'])
    keyword = choice(upload_set['keyword'])

    title = title_prefix % count
    keyword = keyword
    description_prefix = translate('背景音乐: %s - %s 专辑:%s 喜欢我的视频，请快快订阅啦!!!')
    description = description_prefix % (upload_music_info['music_name'], upload_music_info['singer'], upload_music_info['album'])
    upload_cmd = "python yt_upload.py --file '%s' --title '%s' --keywords '%s' --description '%s'" % (final_mp4_output_path, title, keyword, description)
    gLogger.info(upload_cmd)
    os.system(upload_cmd.encode('utf8'))
    config['count'] += 1
    fp = codecs.open('./config.json', 'w+', 'utf-8')
    fp.write(json.dumps(config, ensure_ascii=False, indent=4))
    fp.close()

    # 最后将数据库中发布的文件状态更新
    for pend_item in pend_items:
        id = pend_item['id']
        GifData.update_release_state(id, True)

