#!/usr/bin/python
#coding:utf-8

import opencc


def translate(text,from_lang='zh-cn', to_lang='zh-tw'):
    lang_dic = {'zh-cn': 's', 'zh-tw': 'twp', 'zh-hk': 'hk'}
    if from_lang not in ['zh-cn', 'zh-tw', 'zh-hk'] or to_lang not in ['zh-cn', 'zh-tw', 'zh-hk']:
        raise Exception('语言不识别')
    f_l = lang_dic[from_lang]
    t_l = lang_dic[to_lang]
    reult = opencc.convert(text, config='%s2%s.json' % (f_l, t_l))
    return reult